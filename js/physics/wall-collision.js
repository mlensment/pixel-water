var WallCollision = function() {
  this.reset();
};

WallCollision.prototype.reset = function() {
  this.vector = null;
  this.angle = Math.PI / 2;
  this.corr = 0;
  this.accel = null;
  this.velocity = null;
};

WallCollision.prototype.withDrop = function(a, b, lineRadius, drop) {
  var d = drop.position;
  var circleRadius = drop.radius;

  var ab = new Vector(b.x - a.x, b.y - a.y);
  var ad = new Vector(d.x - a.x, d.y - a.y);

  var ab_ad = ab.x * ad.x + ab.y * ad.y; //scalar

  var cos_alpha = ab_ad / (ab.length() * ad.length());

  //check if circle can even hit the line
  var ac_len = cos_alpha * ad.length();
  var cb_len = ab.length() - ac_len;
  if(ac_len > (ab.length() + lineRadius + circleRadius) || cb_len > (ab.length() + lineRadius + circleRadius)) {
    this.reset();
    return;
  }

  //calculate distance between circle and line
  var alpha = Math.acos(cos_alpha);

  var cd_len = Math.sin(alpha) * ad.length();

  if(cd_len <= circleRadius + lineRadius) {

    var n = new Vector(-ab.y, ab.x); //normal vector
    n.imul(1/n.length()); //convert it to unit vector
    n.imul((circleRadius + lineRadius) - cd_len); //correction vector

    this.vector = ab;
    this.corr = n;
    this.angle = Math.atan2(ab.y, ab.x);

    var motion = this.calcDropMotion(ab, drop);

    this.velocity = motion.velocity;
    return;
  }

  this.reset();
};


WallCollision.prototype.calcDropMotion = function(ab, drop) {
  ab.imul(-1/ab.length());
  ab.imul(drop.velocity.length());
  return {velocity: ab};
};
