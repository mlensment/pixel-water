var Color = function(params) {
  this.color = params.color || 'rgb(0,0,0)';
  this.drops = [new Drop];

  setInterval(function() { this.drip(); }.bind(this), 2000)
};

Color.prototype.drip = function() {
  this.drops.push(new Drop())
};

Color.prototype.update = function() {
  var i = this.drops.length
  while (i--) {
    var drop = this.drops[i];
    drop.update();

    //gc
    if(drop.destroy) {
      this.drops.splice(i, 1);
    }

  }
};

Color.prototype.draw = function() {
  ctx.fillStyle = this.color;

  for(var i in this.drops) {
    var drop = this.drops[i];
    drop.draw();
  }
};
