var PixelWater = function() {
  this.setupCanvas();
  this.blue = new Color({color: 'rgb(0,0,255)'});
  this.wall = new Wall;
  this.phys = new Physics;

  requestAnimationFrame(function() { this.run(); }.bind(this));
};

PixelWater.prototype.run = function() {
  this.update();
  this.draw();
  requestAnimationFrame(function() { this.run(); }.bind(this));
};

PixelWater.prototype.update = function() {
  this.blue.update();
};

PixelWater.prototype.draw = function() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  this.blue.draw();
  this.wall.draw();
};

PixelWater.prototype.setupCanvas = function() {
  $('#pixel-water').prepend('<canvas style="border: 1px solid #424242;" id="pixel-water-canvas" width="' + 500 + '" height="' + 500 + '"></canvas>');
  canvas = document.getElementById('pixel-water-canvas');
  ctx = canvas.getContext('2d');

  ctx.paint = function(pos, lastPos) {
    this.lineWidth = 6;
    this.lineJoin = 'round';
    this.lineCap = 'round';
    this.strokeStyle = 'black';
    this.beginPath();
    this.moveTo(lastPos.x, lastPos.y);
    this.lineTo(pos.x, pos.y);
    this.closePath();
    this.stroke();
  };
};
