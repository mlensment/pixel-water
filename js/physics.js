var Physics = function Physics() {
  this.incline = new Incline();
  this.collision = new Collision();
};

var Incline = function() {
  this.gravityConst = 10;
};

Incline.prototype.calcGravityForce = function(mass) {
  return new Vector(0, mass * this.gravityConst);
};

Incline.prototype.calcParallelForce = function(mass, angle) {
  var gravity = this.calcGravityForce(mass, this.gravityConst);
  var normal = this.vector2D(mass * this.gravityConst * Math.cos(angle), 0.5 * Math.PI - angle, false)
  var force = new Vector(0,0);
  force.iadd(gravity);
  force.iadd(normal);
  return force;
};

Incline.prototype.vector2D = function(mag, angle, clockwise) {
  if(typeof(clockwise) === 'undefined') { clockwise = false; }
  var vec = new Vector(0, 0);
  vec.x = mag * Math.cos(angle);
  vec.y = mag * Math.sin(angle);
  if(!clockwise) { vec.y *= -1; }
  return vec;
};

Incline.prototype.calcAcceleration = function(force, mass) {
  //force.idiv(mass);
  return force;
};

var Collision = function() {

};


// Physics.prototype.calcGravityForce = function(m) {
//
// };

// Physics.prototype.calcParallelForce = function() {

// };

// // COLLISIONS

// Physics.prototype.lineCollisionWithCircle = function() {

// };


// //game.phy.incline.calcGravityForce
