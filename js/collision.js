function lineCircleCollision(a, b, d, circleRadius, lineRadius) {
  var ab = new Vector(b.x - a.x, b.y - a.y);
  var ad = new Vector(d.x - a.x, d.y - a.y);

  var ab_ad = ab.x * ad.x + ab.y * ad.y; //scalar

  var cos_alpha = ab_ad / (ab.length() * ad.length());

  //check if circle can even hit the line
  var ac_len = cos_alpha * ad.length();
  var cb_len = ab.length() - ac_len;
  if(ac_len > (ab.length() + lineRadius + circleRadius) || cb_len > (ab.length() + lineRadius + circleRadius)) {
    return {
      vector: null,
      corr: 0,
      angle: Math.PI / 2
    };
  }

  //calculate distance between circle and line
  var alpha = Math.acos(cos_alpha);

  var cd_len = Math.sin(alpha) * ad.length();

  if(cd_len <= circleRadius + lineRadius) {

    var n = new Vector(-ab.y, ab.x); //normal vector
    n.imul(1/n.length()); //convert it to unit vector
    n.imul((circleRadius + lineRadius) - cd_len); //correction vector

    return {
      vector: ab,
      corr: n,
      angle: Math.atan2(ab.y, ab.x)
    };
  }

  return {
    vector: null,
    corr: 0,
    angle: Math.PI / 2
  };
}
