var Wall = function() {
  //this.path = [];
  //this.path = [{lineStart: true, position: new Vector(90, 20)}, {position: new Vector(110, 30)}, {lineStart: true, position: new Vector(110, 20) }, {position: new Vector(90, 30)}];
  this.path = [
    {lineStart: true, position: new Vector(90, 20)},
    {position: new Vector(250, 200)},
    {lineStart: true, position: new Vector(280, 230)},
    {position: new Vector(100, 400)}
  ];
  this.bindMouse();
};

Wall.prototype.calcAngle = function(a, b) {
  return Math.atan2(b.x - a.x, b.y - a.y);
};

Wall.prototype.draw = function() {
  for(var i in this.path) {
    var path = this.path[i];

    var pos = path.position;
    var lastPos = (i == 0 || path.lineStart) ? pos : this.path[i - 1].position;

    ctx.paint(pos, lastPos)
  };
};

Wall.prototype.bindMouse = function() {
  var self = this;
  canvas.onmousemove = function(e) {
    if (!self.isDrawing) {
       return;
    }

    var x = e.pageX - this.offsetLeft;
    var y = e.pageY - this.offsetTop;

    self.path.push({position: new Vector(x, y), lineStart: self.lineStart});
    self.lineStart = false;
  };

  canvas.onmousedown = function(e) {
    this.lineStart = true;
    this.isDrawing = true;
  }.bind(this);

  canvas.onmouseup = function(e) {
    this.isDrawing = false;
  }.bind(this);
};
